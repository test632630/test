import { FC, useState } from 'react'

interface Param {
	id: number
	name: string
	type: 'string'
}

interface ParamValue {
	paramId: number
	value: string
}

interface Model {
	paramValues: ParamValue[]
}

interface Props {
	params: Param[]
	model: Model
	onParamChange: (paramId: number, value: string) => void
}

// Задаем начальное состояние параметров и модели
const initialParams: Param[] = [
	{ id: 1, name: 'Назначение', type: 'string' },
	{ id: 2, name: 'Длина', type: 'string' },
	{ id: 3, name: 'Вес', type: 'string' }
]
const initialModel: Model = {
	paramValues: [
		{ paramId: 1, value: 'повседневное' },
		{ paramId: 2, value: 'макси' },
		{ paramId: 3, value: '100 кг' }
	]
}

// Компонент для редактирования параметров товара
const ParamEditor: FC<Props> = ({ params, model, onParamChange }) => {
	// Функция для получения значения параметра по его id
	const getParamValue = (paramId: number) => {
		const paramValue = model.paramValues.find(pv => pv.paramId === paramId)
		return paramValue ? paramValue.value : ''
	}

	return (
		<div>
			{params.map(param => (
				<div key={param.id}>
					<label>{param.name}</label>
					<input
						type='text'
						value={getParamValue(param.id)}
						onChange={e => onParamChange(param.id, e.target.value)}
					/>
				</div>
			))}
		</div>
	)
}

const App: React.FC = () => {
	// Состояние для параметров и модели
	const [params, setParams] = useState(initialParams)
	const [model, setModel] = useState(initialModel)

	// Функция для обновления значения параметра
	const handleParamChange = (paramId: number, value: string) => {
		const updatedModel = {
			...model,
			paramValues: model.paramValues.map(paramValue =>
				paramValue.paramId === paramId ? { ...paramValue, value } : paramValue
			)
		}
		setModel(updatedModel)
	}

	return (
		<div
			style={{
				display: 'flex',
				justifyContent: 'center',
				width: '100%'
			}}>
			<ParamEditor params={params} model={model} onParamChange={handleParamChange} />
		</div>
	)
}

export default App
